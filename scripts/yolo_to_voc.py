# yolo_to_json.py (m3-download)
"""
Convert YOLO annotations to a Pascal VOC annotation XMLs
"""
import argparse
import glob
import os
import json
from typing import List
import xml.etree.ElementTree as ETree
from xml.dom import minidom
import imagesize
from pascal_voc_writer import Writer


def convert_yolo_to_voc(yolo_paths: List[str],
                        image_paths: List[str],
                        names: List[str],
                        output_dir: str):
    """Convert a list of YOLO files to VOC XMLs"""
    for yolo_path, image_path in zip(yolo_paths, image_paths):
        with open(yolo_path) as f:
            lines = f.read().splitlines()
            
        # Get image size
        image_width, image_height = imagesize.get(image_path)
        
        writer = Writer(image_path, image_width, image_height)
        
        for line in lines:
            # Extract fields
            fields = line.split()
            concept_idx = int(fields[0])
            x, y, width, height, *_ = tuple(map(float, fields[1:]))
            
            # Shift x, y from center to upper-left
            x -= width / 2
            y -= height / 2
            
            # Scale
            x *= image_width
            y *= image_height
            width *= image_width
            height *= image_height
            
            # Round
            x = round(x)
            y = round(y)
            width = round(width)
            height = round(height)
            
            name = names[concept_idx]
            
            writer.addObject(name, x, y, x + width, y + height)
    
        # Write the file
        output_base = os.path.splitext(os.path.basename(yolo_path))[0] + '.xml'
        output_path = os.path.join(output_dir, output_base)
        writer.save(output_path)


def main(yolo_dir: str, 
         image_dir: str,
         names_file: str, 
         output_dir: str):
    # Load the names file
    if not os.path.isfile(names_file):
        print('[ERROR] Bad names file path: {}'.format(names_file))
        return
    with open(names_file) as f:
        names = f.read().splitlines()
    
    # Check for existence of YOLO directory
    if not os.path.isdir(yolo_dir):
        print('[ERROR] Bad YOLO directory: {}'.format(yolo_dir))
    
    # Check for existence of image directory
    if not os.path.isdir(image_dir):
        print('[ERROR] Bad image directory: {}'.format(image_dir))
        
    # Make the output directory, if it doesn't exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)
        print('[INFO] Created output directory {}'.format(output_dir))

    # Collect all YOLO .txt and image (.png, .jpg) file paths
    yolo_paths = glob.glob(os.path.join(yolo_dir, '*.txt'))
    image_paths = glob.glob(os.path.join(image_dir, '*.png'))
    image_paths.extend(glob.glob(os.path.join(image_dir, '*.jpg')))
    
    if len(yolo_paths) != len(image_paths):
        print('[ERROR] Mismatched number of images ({}) and YOLO annotation files ({})'.format(len(image_paths), len(yolo_paths)))
        return
    
    yolo_paths.sort()
    image_paths.sort()

    # Ensure alignment between yolo paths and image paths
    for yolo_path, image_path in zip(yolo_paths, image_paths):
        assert os.path.splitext(os.path.basename(yolo_path))[0] == os.path.splitext(os.path.basename(image_path))[0]

    print('[INFO] Found {} annotation files and images'.format(len(yolo_paths)))

    # Convert and write
    convert_yolo_to_voc(yolo_paths, image_paths, names, output_dir)


if __name__ == '__main__':
    _parser = argparse.ArgumentParser(description=__doc__)
    _parser.add_argument('yolo_dir',
                         type=str,
                         help='Input directory of YOLO annotation txt files')
    _parser.add_argument('image_dir',
                         type=str,
                         help='Input directory of images')
    _parser.add_argument('names_file',
                         type=str,
                         help='YOLO names file')
    _parser.add_argument('output_dir',
                         type=str,
                         help='Output directory for VOC annotations')
    _args = _parser.parse_args()
    main(_args.yolo_dir, _args.image_dir, _args.names_file, _args.output_dir)
