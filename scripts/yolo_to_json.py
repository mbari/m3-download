# yolo_to_json.py (m3-download)
"""
Convert YOLO annotations to a more portable JSON format
"""
import argparse
import glob
import os
import json
from typing import List


def convert_yolo_to_json(yolo_paths: List[str], names: List[str], 
                         output_file: str, 
                         image_width: int, image_height: int,
                         do_round: bool = True):
    """Convert a list of YOLO files to a portable JSON file"""
    json_content = {}
    for yolo_path in yolo_paths:
        with open(yolo_path) as f:
            lines = f.read().splitlines()
        
        localizations = []
        for line in lines:
            # Extract fields
            fields = line.split()
            concept_idx = int(fields[0])
            x, y, width, height, confidence = tuple(map(float, fields[1:]))
            
            # Shift x, y from center to upper-left
            x -= width / 2
            y -= height / 2
            
            # Scale
            x *= image_width
            y *= image_height
            width *= image_width
            height *= image_height
            
            # Format and append
            localizations.append({
                'concept': names[concept_idx],
                'x': round(x) if do_round else x,
                'y': round(y) if do_round else y,
                'width': round(width) if do_round else width,
                'height': round(height) if do_round else height
            })
        
        json_content[yolo_path] = localizations
    
    # Write the file
    with open(output_file, 'w') as f:
        json.dump(json_content, f, indent=2)


def main(input_dir: str, names_file: str, 
         output_file: str, 
         width: int, height: int, 
         round: bool):
    # Load the names file
    if not os.path.isfile(names_file):
        print('[ERROR] Bad names file path: {}'.format(names_file))
        return
    with open(names_file) as f:
        names = f.read().splitlines()
    
    # Check for existence of input directory
    if not os.path.isdir(input_dir):
        print('[ERROR] Bad input directory: {}'.format(input_dir))

    # Collect all YOLO txt file paths in all directories
    yolo_paths = glob.glob(os.path.join(input_dir, '*.txt'))
    yolo_paths.sort()

    print('[INFO] Found {} annotation files'.format(len(yolo_paths)))

    # Convert and write
    convert_yolo_to_json(yolo_paths, names, output_file, width, height, do_round=round)


if __name__ == '__main__':
    _parser = argparse.ArgumentParser(description=__doc__)
    _parser.add_argument('input_dir',
                         type=str,
                         help='Input directory of YOLO annotation txt files')
    _parser.add_argument('names_file',
                         type=str,
                         help='YOLO names file')
    _parser.add_argument('width',
                         type=int,
                         help='Image width (for scaling)')
    _parser.add_argument('height',
                         type=int,
                         help='Image height (for scaling)')
    _parser.add_argument('output_file',
                         type=str,
                         help='Output JSON file name')
    _parser.add_argument('-r', '--round',
                         action='store_true',
                         dest='round',
                         help='Round image coordinates to the nearest whole number')
    _args = _parser.parse_args()
    main(_args.input_dir, _args.names_file, _args.output_file, _args.width, _args.height, bool(_args.round))
