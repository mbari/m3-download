"""
Find and report Pascal VOC files with "duplicate" bounding boxes: boxes with an IoU threshold greater than a specified value.
"""

import argparse
import xml.etree.ElementTree as ETree
from pathlib import Path
from typing import Iterable, Union

from boxdiff import BoundingBox


def parse_voc_to_boxes(voc_path_str: Union[Path, str]) -> Iterable[BoundingBox]:
    """
    Parse a Pascal VOC XML file into box-diff BoundingBoxes.
    """
    tree = ETree.parse(str(voc_path_str))
    root = tree.getroot()

    objects = root.findall('object')
    for loc in objects:
        name = loc.find('name').text
        bndbox = loc.find('bndbox')
        xmin = float(bndbox.find('xmin').text)
        xmax = float(bndbox.find('xmax').text)
        ymin = float(bndbox.find('ymin').text)
        ymax = float(bndbox.find('ymax').text)
        
        yield BoundingBox(0, name, xmin, ymin, xmax - xmin, ymax - ymin)


def find_duplicate_boxes(boxes: Iterable[BoundingBox], threshold: float, label_match: bool = False) -> Iterable[BoundingBox]:
    """
    Find duplicate boxes above a specified threshold.
    """
    boxes = list(boxes)
    for box in boxes:
        for other in boxes:
            if box == other:
                continue
            if box.iou(other) > threshold:
                if not label_match or box.label == other.label:
                    yield box


def check_voc_file(voc_path: Path, threshold: float, label_match: bool = False) -> bool:
    """
    Check if a VOC file has duplicate bounding boxes.
    """
    boxes = parse_voc_to_boxes(voc_path)
    return bool(list(find_duplicate_boxes(boxes, threshold, label_match=label_match)))


def find_duplicate_voc_files(voc_paths: Iterable[Path], threshold: float, label_match: bool = False) -> Iterable[Path]:
    """
    Find VOC files with duplicate bounding boxes.
    """
    for voc_path in voc_paths:
        if check_voc_file(voc_path, threshold, label_match=label_match):
            yield voc_path


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('voc_dir', type=Path, help='VOC directory')
    parser.add_argument('threshold', type=float, help='IoU threshold')
    parser.add_argument('--label-match', action='store_true', help='Only consider boxes with matching labels as duplicates')
    
    args = parser.parse_args()
    
    voc_dir: Path = args.voc_dir
    threshold: float = min(max(args.threshold, 0), 1)
    
    if not args.voc_dir.is_dir():
        parser.error(f'{voc_dir} does not exist or is not a directory')
    
    voc_xml_files = list(voc_dir.glob('*.xml'))
    
    if not voc_xml_files:
        parser.error('No XML files found in VOC directory')
    
    # Check and print out VOC files with duplicate bounding boxes
    for voc_xml_file in find_duplicate_voc_files(voc_xml_files, threshold, label_match=args.label_match):
        print(voc_xml_file)


if __name__ == '__main__':
    main()
