# utils.py (m3-download)

from datetime import datetime, timedelta
from typing import Optional, Tuple


def error(msg):
    print('[ERROR] {}'.format(msg))


def warning(msg):
    print('[WARNING] {}'.format(msg))


def parse_iso(timestamp: str) -> datetime:
    """
    Parse an ISO timestamp.
    
    Args:
        timestamp: The timestamp to parse.
    
    Returns:
        The parsed timestamp.
    """
    try:
        return datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    except ValueError:
        return datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")


def get_timestamp(video_start_timestamp: datetime, recorded_timestamp: Optional[datetime], elapsed_time_millis: Optional[int] = None, timecode: Optional[str] = None) -> Optional[datetime]:
    """
    Get a timestamp from the given parameters. One of the following must be provided:
    - recorded_timestamp
    - elapsed_time_millis
    - timecode
    or else None will be returned.
    
    Args:
        video_start_timestamp: The video's start timestamp.
        recorded_timestamp: The recorded timestamp.
        elapsed_time_millis: The elapsed time in milliseconds.
        timecode: The timecode.
    
    Returns:
        The timestamp, or None if none could be determined.
    """             
    # First, try to use the recorded timestamp (microsecond resolution)
    if recorded_timestamp is not None:
        return recorded_timestamp
    
    # Next, try to use the elapsed time in milliseconds (millisecond resolution)
    elif elapsed_time_millis is not None:
        return video_start_timestamp + timedelta(
            milliseconds=int(elapsed_time_millis)
        )
    
    # Last, try to use the timecode (second resolution)
    elif timecode is not None:
        splitter = ":" if ":" in timecode else "-"
        hours, minutes, seconds, _ = map(int, timecode.split(splitter))
        return video_start_timestamp + timedelta(
            hours=hours, minutes=minutes, seconds=seconds
        )
    
    # If none of the above worked, return None
    return None


def find_video_with_mp4_reference(video_sequence_data: dict, dt: datetime) -> Optional[Tuple[dict, dict]]:
    """
    Find a video that contains the given datetime and an MP4 video reference for that video.
    
    Args:
        video_sequence_data: The video sequence data.
        dt: The datetime.
    """
    videos = video_sequence_data.get("videos", [])
    for video in videos:
        video_duration_millis = video.get("duration_millis", None)
        if video_duration_millis is None:
            continue
        
        video_start_timestamp = video.get("start_timestamp", None)
        if video_start_timestamp is None:
            continue
        
        # Compute datetime start-end range
        video_start_datetime = parse_iso(video_start_timestamp)
        video_end_datetime = video_start_datetime + timedelta(milliseconds=video_duration_millis)
        
        if not (video_start_datetime <= dt <= video_end_datetime):  # Datetime not in range
            continue
        
        # Video contains timestamp; look for an MP4 video reference
        video_references = video.get("video_references", [])
        for video_reference in video_references:
            video_reference_container = video_reference.get("container", None)
            if video_reference_container is None or video_reference_container != "video/mp4":
                continue
            
            return video, video_reference
