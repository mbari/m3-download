"""
Data models for the m3-download pipeline.
"""

from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import List, Optional, Tuple, Union
from uuid import UUID
from os.path import basename
from urllib.parse import urlparse
from progressbar import ProgressBar

import requests
import imagesize
from dataclasses_json import dataclass_json
from lxml import etree
from lxml.builder import E
from beholder_client.client import BeholderClient


@dataclass_json
@dataclass
class Concept:
    """
    Concept = label + part label
    """
    concept: str
    part: Optional[str] = None
    
    def __str__(self):
        return f'{self.concept}' + (f' {self.part}' if self.part else '')


@dataclass
class Constraints:
    """
    Query constraints
    """
    concepts: Optional[List[Concept]] = None
    limit: Optional[int] = None


@dataclass_json
@dataclass
class BoundingBox:
    """
    Bounding box
    """
    x: float
    y: float
    width: float
    height: float
    
    def to_points(self) -> Tuple[float]:
        """
        Convert to list of points
        """
        return self.x, self.y, self.x + self.width, self.y + self.height

    @classmethod
    def from_points(cls, points: Tuple[float]) -> 'BoundingBox':
        """
        Convert from list of points
        """
        return cls(points[0], points[1], points[2] - points[0], points[3] - points[1])


@dataclass_json
@dataclass
class Observation:
    """
    Observation
    """
    uuid: UUID
    concept: Concept
    activity: str
    group: str


@dataclass_json
@dataclass
class Localization:
    """
    Localization = observation + bounding box
    """
    observation: Observation
    bounding_box: BoundingBox


@dataclass_json
@dataclass
class Image:
    """
    Image
    """
    video_sequence_name: str
    timestamp: datetime
    width: int
    height: int
    url: Optional[str]
    localizations: List[Localization]
    
    def download(self, path: Union[str, Path]) -> Optional['SavedImage']:
        """
        Download the image to the given path.
        """
        try:
            with requests.get(self.url, stream=True) as r:
                r.raise_for_status()
                with Path(path).open('wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        f.write(chunk)
        except requests.HTTPError as e:
            print(f'Failed to download image {self.url}: {e}')
            return None
        
        return SavedImage(self.video_sequence_name, self.timestamp, self.width, self.height, self.url, self.localizations, path)


@dataclass
class VideoFrame(Image):
    """
    Video frame. Extends an image with necessary info to capture on-the-fly with Beholder.
    """
    beholder_client: BeholderClient
    video_url: str
    elapsed_time_millis: int
    
    def download(self, path: Union[str, Path]) -> Optional['SavedImage']:
        """
        Capture the image with Beholder and save to a given path.
        """
        try:
            image = self.beholder_client.capture(self.video_url, self.elapsed_time_millis)
            image.save(path)
        except Exception as e:
            print(f'Failed to capture image from {self.video_url} at {self.elapsed_time_millis} ms: {e}')
            return None
        
        return SavedImage(self.video_sequence_name, self.timestamp, self.width, self.height, self.url, self.localizations, path)


@dataclass_json
@dataclass
class Dataset:
    """
    Dataset
    """
    images: List[Image]
    
    def download(self, output_dir: Union[str, Path], overwrite: bool = False, progressbar: Optional[ProgressBar] = None) -> 'SavedDataset':
        """
        Download images to a target directory. 
        
        If overwrite is True, existing files will be overwritten.
        
        Args:
            output_dir: Target directory
            overwrite: Overwrite existing files
            progressbar: Progress bar
        """
        output_dir = Path(output_dir)
        
        images = self.images if progressbar is None else progressbar(self.images)
        
        saved_images = []
        for image in images:
            # Format the filename
            filename = f'{image.video_sequence_name}_{image.timestamp.strftime("%Y%m%d_%H%M%S_%f")}.jpg'
            
            # If the URL is set, use that file extension
            if image.url is not None:
                extension = Path(urlparse(image.url).path).suffix
                filename = filename[:-4] + extension
            
            path = output_dir / filename
            
            # Check if file already exists
            if path.exists() and not overwrite:
                saved_image = SavedImage(image.video_sequence_name, image.timestamp, image.width, image.height, image.url, image.localizations, path)
            else:
                saved_image = image.download(path)  # Download image
            
            if saved_image is not None:  # Ensure download was successful
                # Correct image size
                saved_image.correct_size()
                
                # Append to list of saved images
                saved_images.append(saved_image)
        
        return SavedDataset(saved_images)
    
    def save_json(self, filename: Union[str, Path], **kwargs):
        """
        Save dataset to file in JSON format
        """
        with Path(filename).open('w') as f:
            f.write(self.to_json(default=str, **kwargs))
    
    @classmethod
    def load_json(cls, filename: Union[str, Path]):
        """
        Load dataset from file in JSON format
        """
        with Path(filename).open('r') as f:
            return cls.from_json(f.read())


@dataclass_json
@dataclass
class SavedImage(Image):
    """
    Saved image
    """
    path: Path
    
    def to_voc(self, pretty_print: bool = False) -> str:
        """
        Convert to VOC format
        """
        # Encode localizations into object tags
        objects = [
            E.object(
                E.name(str(localization.observation.concept)),
                E.pose('Unspecified'),
                E.truncated('0'),
                E.difficult('0'),
                E.occluded('0'),
                E.bndbox(
                    E.xmin(str(int(localization.bounding_box.x))),
                    E.xmax(str(int(localization.bounding_box.x + localization.bounding_box.width))),
                    E.ymin(str(int(localization.bounding_box.y))),
                    E.ymax(str(int(localization.bounding_box.y + localization.bounding_box.height)))
                )
            )
            for localization in self.localizations
        ]

        # Encode annotation data
        folder = self.path.parent.name
        filename = self.path.name
        path = str(self.path)
        annotation = E.annotation(
            E.folder(folder),
            E.filename(filename),
            E.path(path),
            E.source(
                E.database('VARS')
            ),
            E.size(
                E.width(str(self.width)),
                E.height(str(self.height)),
                E.depth('3')
            ),
            E.segmented('0'),
            *objects
        )

        return etree.tostring(annotation, pretty_print=pretty_print).decode()
    
    def correct_size(self):
        """
        Correct the image size from the file
        """
        width, height = imagesize.get(str(self.path))
        self.width = width
        self.height = height


@dataclass_json
@dataclass
class SavedDataset(Dataset):
    """
    Dataset with images saved to disk
    """
    images: List[SavedImage]
    
    def save_voc(self, output_dir: Union[str, Path], pretty_print: bool = False):
        """
        Save dataset to a directory in VOC format
        """
        output_dir = Path(output_dir)
        
        for image in self.images:
            path = output_dir / image.path.with_suffix('.xml').name
            with path.open('w') as f:
                f.write(image.to_voc(pretty_print=pretty_print))
