"""
Download all localizations and corresponding images from M3.
"""

import argparse
from dataclasses import dataclass
from getpass import getpass
import json
from pathlib import Path
from typing import Dict, Iterable, List, Optional
from uuid import UUID

import pymssql
from progressbar import ProgressBar
from beholder_client.client import BeholderClient

from lib.m3.clients import RazielClient, VampireSquidClient
from lib.models import BoundingBox, Concept, Dataset, Image, Localization, Observation, VideoFrame
from lib.utils import error, find_video_with_mp4_reference, get_timestamp, parse_iso, warning


@dataclass
class BoundingBoxPart:
    x: int = None
    y: int = None
    width: int = None
    height: int = None


@dataclass
class LocalizationRow:
    association_uuid: UUID
    observation_uuid: UUID
    image_reference_uuid: Optional[UUID]
    concept: str
    to_concept: str
    bounding_box: BoundingBoxPart
    image_url: str
    activity: str
    group: str
    recorded_timestamp: Optional[str]
    elapsed_time_millis: Optional[int]
    timecode: Optional[str]
    video_sequence_name: str
    video_start_timestamp: str


def parse_localization_row(row: tuple) -> LocalizationRow:
    """
    Parse a row in the query response into a LocalizationRow.
    """
    d = json.loads(row[5])
    return LocalizationRow(
        association_uuid=row[0],
        observation_uuid=row[1],
        image_reference_uuid=row[2],
        concept=row[3],
        to_concept=row[4],
        bounding_box=BoundingBoxPart(*[d[k] for k in ('x', 'y', 'width', 'height')]),
        image_url=row[6],
        activity=row[7],
        group=row[8],
        recorded_timestamp=row[9],
        elapsed_time_millis=row[10],
        timecode=row[11],
        video_sequence_name=row[12],
        video_start_timestamp=row[13],
    )


def encode_dataset(localization_rows: Iterable[LocalizationRow], video_sequences_by_name: Dict[str, Optional[dict]], beholder_client: BeholderClient, groups: Optional[List[str]] = None, activities: Optional[List[str]] = None) -> Dataset:
    """
    Encode the dataset from the given rows.
    
    Args:
        localization_rows: Iterable of LocalizationRow objects.
        video_sequences_by_name: Dict of video sequence data by video sequence name.
        beholder_client: Beholder client.
        groups: List of groups to include. If None, include all groups.
        activities: List of activities to include. If None, include all activities.
    
    Returns:
        Dataset object.
    """
    # Maintain map of (video sequence name, datetime) -> Image
    image_map = {}

    # Collect localization rows into Images
    for row in localization_rows:
        # Filter by group and activity
        if groups is not None and row.group not in groups:
            continue
        if activities is not None and row.activity not in activities:
            continue
        
        # Get the video sequence data
        video_sequence_data = video_sequences_by_name.get(row.video_sequence_name, None)
        if video_sequence_data is None and row.image_url is None:  # Skip if video sequence data is not available, and we don't have a URL
            continue
        if row.video_start_timestamp is None:  # Skip if video start timestamp is not available
            continue
        
        # Get the imaged moment datetime
        imaged_moment_datetime = get_timestamp(
            row.video_start_timestamp,
            row.recorded_timestamp,
            row.elapsed_time_millis,
            row.timecode
        )
        if imaged_moment_datetime is None:  # Skip if imaged moment datetime is not available
            continue
        
        # Create the Localization
        localization = Localization(
            Observation(
                row.observation_uuid,
                Concept(
                    row.concept, row.to_concept if row.to_concept != 'self' else None
                ),
                row.activity,
                row.group,
            ),
            BoundingBox(
                row.bounding_box.x,
                row.bounding_box.y,
                row.bounding_box.width,
                row.bounding_box.height,
            ),
        )

        # Create the Image if it doesn't exist
        key = (row.video_sequence_name, imaged_moment_datetime)
        if key not in image_map:
            image = None
            if row.image_url is not None:
                image = Image(
                    row.video_sequence_name, imaged_moment_datetime, 0, 0, row.image_url, []
                )
            else:
                # Find an MP4 video reference in the video sequence data that contains the imaged moment datetime
                opt = find_video_with_mp4_reference(video_sequence_data, imaged_moment_datetime)
                if opt is None:  # Skip if no MP4 video reference is found
                    continue
            
                video, video_reference = opt
                
                elapsed_time_millis = round((imaged_moment_datetime - parse_iso(video["start_timestamp"])).total_seconds() * 1000)
                image = VideoFrame(
                    row.video_sequence_name, imaged_moment_datetime, 0, 0, row.image_url, [], beholder_client, video_reference["uri"], elapsed_time_millis
                )
            
            image_map[key] = image

        # Add the Localization to the Image
        image_map[key].localizations.append(localization)

    # Compose the dataset
    dataset = Dataset(image_map.values())

    return dataset


def get_video_sequences_by_name(vampire_squid_client: VampireSquidClient, video_sequence_names: Iterable[str]) -> List[dict]:
    """
    Get video sequence data for the given video sequence names.
    
    Args:
        vampire_squid_client: VampireSquidClient object.
        video_sequence_names: Iterable of video sequence names.
    
    Returns:
        List of video sequence data dicts.
    """
    video_sequences_by_name = {}
    
    for video_sequence_name in video_sequence_names:
        if video_sequence_name not in video_sequences_by_name:
            # Try to fetch
            try:
                video_sequence_data = vampire_squid_client.get_video_sequence_by_name(video_sequence_name).json()
            except Exception as e:
                error(f"Failed to get video sequence data for {video_sequence_name}: {e}")
                video_sequence_data = None
            
            # Store in dict
            video_sequences_by_name[video_sequence_name] = video_sequence_data
    
    return video_sequences_by_name


def get_raziel_client(url: str) -> Optional[RazielClient]:
    """
    Prompt for Raziel credentials and return a RazielClient.
    
    Args:
        url: Raziel URL.
    
    Returns:
        RazielClient object if successful, otherwise None.
    """
    # Get user/pass
    try:
        username = input('Username: ')
        password = getpass('Password: ')
    except KeyboardInterrupt:
        return None
    
    client = RazielClient(url)
    
    # Authenticate the client
    try:
        client.authenticate(username, password)
    except Exception as e:
        error(f"Failed to authenticate: {e}")
        return None

    return client


def get_beholder_client(endpoint: dict) -> BeholderClient:
    """
    Get a BeholderClient from the given endpoint data dict.
    
    Args:
        endpoint: Endpoint data dict.
    
    Returns:
        BeholderClient object.
    """
    url = endpoint["url"]
    secret = endpoint["secret"]
    
    return BeholderClient(url, secret)


def get_vampire_squid_client(endpoint: dict) -> VampireSquidClient:
    """
    Get a VampireSquidClient from the given endpoint data dict.
    
    Args:
        endpoint: Endpoint data dict.
    
    Returns:
        VampireSquidClient object.
    """
    url = endpoint["url"]
    
    return VampireSquidClient(url)


def main():
    def comma_list(s: str) -> List[str]:
        return s.split(',')
    
    step = 1
    n_steps = 6
    def print_step(message: str):
        nonlocal step
        print(f"[{step}/{n_steps}] {message}")
        step += 1
    
    # Set up the command line argument parser
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--host', default='perseus.shore.mbari.org', help='MSSQL host. Default: %(default)s')
    parser.add_argument('--user', default='everyone', help='MSSQL user. Default: %(default)s')
    parser.add_argument('--password', default='guest', help='MSSQL password. Default: %(default)s')
    parser.add_argument('--database', default='M3_ANNOTATIONS', help='MSSQL database. Default: %(default)s')
    parser.add_argument('--config', default='http://m3.shore.mbari.org/config', help='Raziel config URL. Default: %(default)s')
    parser.add_argument('--pretty', action='store_true', help='Pretty-print the XML output.')
    parser.add_argument('--groups', type=comma_list, default=[], help='Comma-separated list of groups to download. Default: all groups')
    parser.add_argument('--activities', type=comma_list, default=[], help='Comma-separated list of activities to download. Default: all activities')
    parser.add_argument('image_dir', type=Path, help='Directory to save images to.')
    parser.add_argument('xml_dir', type=Path, help='Directory to save Pascal VOC XML files to.')
    
    # Parse args
    args = parser.parse_args()
    
    # Ensure image and xml directories are ready to write
    try:
        args.image_dir.mkdir(parents=True, exist_ok=True)
    except FileExistsError:
        parser.error(f'{args.image_dir} already exists and is not a directory.')
    try:
        args.xml_dir.mkdir(parents=True, exist_ok=True)
    except FileExistsError:
        parser.error(f'{args.xml_dir} already exists and is not a directory.')
    
    # Get the Raziel client
    raziel_client = get_raziel_client(args.config)
    if raziel_client is None:
        parser.error('Failed to authenticate.')
    
    endpoints = raziel_client.get_endpoints().json()
    endpoints = {endpoint['name']: endpoint for endpoint in endpoints}  # index by name
    
    # Get the Beholder client
    beholder_client = get_beholder_client(endpoints["beholder"])
    
    # Get the Vampire Squid client
    vampire_squid_client = get_vampire_squid_client(endpoints["vampire-squid"])
    
    try:
        # Connect to MSSQL server
        print_step(f'Connecting to {args.host}...')
        try:
            conn = pymssql.connect(args.host, args.user, args.password, args.database, login_timeout=5)
            cursor = conn.cursor()
        except pymssql.OperationalError as e:
            parser.error(f'Could not connect to MSSQL server at {args.host}: {e}')
        print('Connected.\n')

        # Read the query
        with open('query.sql') as f:
            query_str = f.read()

        # Execute query
        print_step('Executing query...')
        cursor.execute(query_str)

        # Fetch rows
        rows = cursor.fetchall()
        conn.close()
        print(f'Fetched {len(rows)} rows.\n')

        # Parse rows
        localization_rows = [parse_localization_row(row) for row in rows]
        
        # Get the set of video sequence names
        video_sequence_names = set(row.video_sequence_name for row in localization_rows if row.image_url is None)
        
        # Get video sequence data
        print_step(f'Fetching information for {len(video_sequence_names)} video sequences...')
        video_sequences_by_name = get_video_sequences_by_name(vampire_squid_client, video_sequence_names)
        print(f'Fetched {len(video_sequences_by_name)} video sequences.\n')

        # Encode the dataset
        print_step('Encoding dataset...')
        dataset = encode_dataset(
            localization_rows, 
            video_sequences_by_name,
            beholder_client,
            groups=args.groups if args.groups else None,  # If no groups specified, include all groups
            activities=args.activities if args.activities else None  # If no activities specified, include all activities
        )
        print(f'Encoded {len(dataset.images)} images with {sum(len(image.localizations) for image in dataset.images)} localizations.\n')

        # Download images
        print_step('Downloading images (this may take a while)...')
        with ProgressBar(max_value=len(dataset.images), redirect_stdout=True) as progressbar:
            downloaded_dataset = dataset.download(args.image_dir, progressbar=progressbar)
        print(f'Downloaded {len(downloaded_dataset.images)} images.\n')

        # Save the dataset
        print_step('Writing dataset to disk...')
        downloaded_dataset.save_voc(args.xml_dir, pretty_print=args.pretty)

        print('Finished!')
    
    except KeyboardInterrupt:
        print('Aborted.')


if __name__ == '__main__':
    main()
