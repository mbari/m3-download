SELECT
	a.uuid AS association_uuid, 
	o.uuid AS observation_uuid, 
	ir.uuid AS image_reference_uuid, 
	o.concept, 
	a.to_concept, 
	a.link_value, 
	ir.url, 
	o.activity, 
	o.observation_group,
	im.recorded_timestamp,
	im.elapsed_time_millis,
	im.timecode,
	vs.name,
	v.start_time
FROM
	M3_ANNOTATIONS.dbo.associations a INNER JOIN
	M3_ANNOTATIONS.dbo.observations o ON a.observation_uuid = o.uuid INNER JOIN 
	M3_ANNOTATIONS.dbo.imaged_moments im ON o.imaged_moment_uuid = im.uuid INNER JOIN
	M3_VIDEO_ASSETS.dbo.video_references vr ON im.video_reference_uuid = vr.uuid INNER JOIN
	M3_VIDEO_ASSETS.dbo.videos v ON vr.video_uuid = v.uuid INNER JOIN
	M3_VIDEO_ASSETS.dbo.video_sequences vs ON v.video_sequence_uuid = vs.uuid LEFT JOIN 
	M3_ANNOTATIONS.dbo.image_references ir ON JSON_VALUE(a.link_value, '$.image_reference_uuid') = ir.uuid
WHERE
	a.link_name = 'bounding box'